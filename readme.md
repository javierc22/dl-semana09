## Desafío Latam - Semana 9

#### Objetos:

1. Introducción
2. Motivación y conceptos claves
3. Nuestro primer objeto
4. Class
5. Método de instancia
6. Variables de instancia
7. Scope de variables de instancia
8. Ejemplo de métodos y atributos

#### Objetos avanzados:

9. Constructor
10. Intro a 'getters' y 'setters'
11. El misterio del perro sin edad
12. Getters y Setters
13. Ejercicios de Getters y Setters
14. Mini Quiz
15. Estudiante promedio
16. Creando una baraja
17. Quiz

#### Objetos y arreglos:

18. Arreglo de productos
19. Intro a ejercicio 'estudiantes'
20. Solución ejercicio 'estudiantes'
21. Promedio de los promedios
22. Intro al objeto 'Date'
23. Objetos, archivos y fechas
24. Quiz
