# -------- METODOS DE INSTANCIA --------- #
# Podemos definir comportamientos agregando métodos dentro de una clase.
# Este tipo de método recibe el nombre de 'métodos de instancia'.

class Zombie
  def saludar
    puts 'braaaaains'
  end
end

zombie = Zombie.new
zombie.saludar