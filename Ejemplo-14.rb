# ----------- EJERCICIO: ESTUDIANTES ------------ #
# Crear la clase Student, cada alumno tiene un nombre y notas del 0 al 10.
# Se pide:
# - Leer el archivo y crear un arreglo de alumnos con la información de sus notas
# - Crear un método para calcular el promedio
# - Mostrar el promedio de notas de todos los alumnos.

# * = splat

class Student
  attr_reader :name
  def initialize(name, *scores)
    @name = name
    @scores = scores.map(&:to_i)
  end

  def average
    @scores.inject(&:+) / @scores.size.to_f
  end
end

# Leyendo archivo:
data = []
File.open('Ejemplo-14.txt', 'r') { |file| data = file.readlines.map(&:chomp) }

students = []
data.each do |line|
  ls = line.split(', ')
  students << Student.new(*ls) # students => @name="Juan Pablo", @scores=[10, 7, 9], @name="Magdalena", @scores=[9, 9, 9], ...
end

# Promedio de cada alumno:
students.each do |student|
  puts "#{student.name} = #{student.average.round(2)}" # round(2) = redondear a 2 decimales.
end

# Promedio total:
averages = students.map(&:average) # averages => [8.666666666666666, 9.0, 7.0]
total = averages.inject(&:+) / averages.size.to_f # inject(&:+) = suma todo lo contenido en el arreglo
puts "Promedio total => #{total.round(2)}" # round(2) = redondear a 2 decimales.