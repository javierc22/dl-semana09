# -------- método CONSTRUCTOR --------- #
class Persona
  def initialize(edad) # constructor
    @edad = edad
  end

  def envejercer
    @edad += 1
  end

  def mostrar_edad
    puts @edad
  end
end

persona1 = Persona.new(10)

10.times do
  persona1.envejercer
end

persona1.mostrar_edad # => 20