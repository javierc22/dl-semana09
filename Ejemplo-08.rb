# -------- INTRO A GETTERS Y SETTERS --------- #
# attr_accessor nos permite acceder a la variable @edad desde afuera
# crea un método para obtener la edad y otro método para modificarlo.
class Persona
  attr_accessor :edad
  def initialize(edad) # constructor
    @edad = edad
  end

  def envejercer
    @edad += 1
  end
end

persona1 = Persona.new(10)
persona1.edad = 41
puts persona1.edad # => 41