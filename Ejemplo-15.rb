# ----------- EJERCICIO: OBJETOS, ARCHIVOS Y FECHAS ---------- #
# Se pide:
# - Crear el objeto Empresa y almacenar los pagos como arreglo
# - Crear un método que muestre todos los pagos antes de una fecha
# - Crear un método que muestre todos los pagos registrados
# posteriores al del día de hoy
require 'date'

class Company
  def initialize(name, *payments)
    @name = name
    @payments = payments.map { |date| Date.parse(date) } # Convierte las fechas a formato de tipo DATE
  end

  def payments_before(filter_date)
    # Levanta una excepción en caso que el argumento no sea una fecha.
    raise ArgumentError.new('Argument is not date') if filter_date.class != Date
    @payments.select { |date| date < filter_date}
  end

  def payments_after(filter_date)
    # Levanta una excepción en caso que el argumento no sea una fecha.
    raise ArgumentError.new('Argument is not date') if filter_date.class != Date 
    @payments.select { |date| date > filter_date}
  end
end

# Leyendo archivo:
data = []
File.open('Ejemplo-15.txt', 'r') { |file| data = file.readlines.map(&:chomp) }

companies = []
data.each do |line|
  ls = line.split(' ')
  companies << Company.new(*ls) # companies => @name="Empresa1", @payments=[2016-05-21, 2017-08-10, ...], ...
end

# Pagos antes de una fecha:
puts 'Pagos antes de la fecha:'
print companies[2].payments_before(Date.today)

# Pagos después de una fecha
puts
puts 'Pagos después de la fecha'
print companies[2].payments_after(Date.today)