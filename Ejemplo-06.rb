# -------- EJEMPLO CON METODOS Y ATRIBUTOS --------- #
class Persona
  def nacer
    @edad = 0
  end

  def envejercer
    @edad += 1
  end

  def mostrar_edad
    puts @edad
  end
end

persona1 = Persona.new
persona1.nacer

10.times do
  persona1.envejercer
end

persona1.mostrar_edad # => 10