# -------- EJERCICIO GETTERS Y SETTERS ----------- #
# Modificar el código para acceder al nombre y modificar las notas

class Alumno
  attr_reader :nombre, :notas
  def initialize()
    @notas = [7, 'A', 'B+', 5]
    @nombre = 'Javier'
  end
end

puts Alumno.new.nombre # => Javier
print Alumno.new.notas # => [7, "A", "B+", 5]

