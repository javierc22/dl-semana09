# -------- GETTERS Y SETTERS --------- #
# Nos permite acceder a la variable @edad desde afuera
# y crea un método para obtener la edad y otro método para modificarlo.

# ------------------------------------------------------------------- #
# Ejemplo 1:
class Perro1
  def initialize(edad) # constructor
    @edad = edad
  end

  # getter
  def get_edad
    @edad
  end

  # setter
  def set_edad(edad)
    @edad = edad
  end
end

perro1 = Perro1.new 12
puts perro1.get_edad # => 12

perro1.set_edad(20)
puts perro1.get_edad # => 20

# ------------------------------------------------------------------- #
# Ejemplo 2: Getters
class Perro2
  attr_reader :edad # Obtener la edad
  def initialize(edad) # constructor
    @edad = edad
  end

  def set_edad(edad)
    @edad = edad
  end
end

perro2 = Perro2.new 12
puts perro2.edad # => 12

# ------------------------------------------------------------------- #
# Ejemplo 3: Setters
class Perro3
  attr_writer :edad # setear la edad
  def initialize(edad) # constructor
    @edad = edad
  end

  def get_edad
    @edad
  end
end

perro3 = Perro3.new 21
perro3.edad = 14
puts perro3.get_edad # => 14

# ------------------------------------------------------------------- #
# Ejemplo 4: Getters y Setters
class Perro4
  attr_accessor :edad # obtener y setear la edad
  def initialize(edad) # constructor
    @edad = edad
  end
end

perro4 = Perro4.new 5
perro4.edad = 10
puts perro4.edad # => 10