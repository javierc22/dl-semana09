# ------ SCOPE DE VARIABLES DE INSTANCIA ------- #
class Ejemplo
  def valores_iniciales()
    @var_de_instancia = 5
    var_local = 9
  end

  def mostrar_valores()
    puts @var_de_instancia # => 5
    puts var_local # => error : undefined local variable
  end
end

ejemplo = Ejemplo.new()
ejemplo.valores_iniciales
ejemplo.mostrar_valores