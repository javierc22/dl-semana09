# ------- VARIABLES DE INSTANCIA -------- #
# Los atributos son variables de instancias, estás se identifican porque empiezan con @
# Las variables de instancia, dependen de la instancia

class Persona
  def bautizar(nuevo_nombre)
    @nombre = nuevo_nombre
  end

  def saludar()
    puts "#{@nombre} dice Hola!"
  end
end

persona1 = Persona.new
persona1.bautizar('Jaime')

persona2 = Persona.new
persona2.bautizar('Francisca')

persona1.saludar()
persona2.saludar()
