# ------------- EJERCICIO: ARREGLO DE PRODUCTOS ------------- #
# Crear la clase Product, cada producto tiene stock asociados a cada color,
# donde son 3 los posibles colores, azul, rojo, y verde

# Se pide:

class Product
  attr_reader :name, :blue, :red, :green
  def initialize(name, blue, red, green)
    @name = name
    @blue = blue.to_i
    @red = red.to_i
    @green = green.to_i
  end

  def stock_total
    @blue + @red +@green
  end
end

# 1. Leer el archivo y crear un arreglo de productos con la información.
data = []
File.open('Ejemplo-13.txt', 'r') { |file| data = file.readlines.map(&:chomp) }

products = []
data.each do |line|
  name, blue, red, green = line.split(', ')
  products << Product.new(name, blue, red, green)
end

# 2. Mostrar el stock de cada productos en color rojo.
puts 'Stock de cada producto en color rojo:'
products.each { |product| puts product.red }

# 3. Mostrar el total de stock rojo (la suma de todos los rojo de cada producto).
puts 'Stock total de stock rojo:'
puts products.inject(0) { |sum, product| sum + product.red }

# 4. Mostrar el stock total de un producto.


# 5. Mostrar el stock total de todos los productos.
puts 'Stock total:'
puts products.inject(0) { |sum, product| sum + product.stock_total }