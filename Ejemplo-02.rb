# --------------- CLASS ---------------- #
# Para saber de que clase es un objeto, podemos utilizar el método .class

# 1.
a = []
puts a.class # => Array

# 2.
b = {}
puts b.class # => Hash

# 3.
class Lego
end

lego = Lego.new
puts lego.class # => Lego